﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ePatient.Startup))]
namespace ePatient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
