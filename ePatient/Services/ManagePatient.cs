﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ePatient.DAL;
using ePatient.Models;
using System.Globalization;
using System.Data.Entity;
using System.Data.Entity.Migrations;


namespace ePatient.Services
{
    public class ManagePatient:SingletonHelper<ManagePatient>
    {
        private ManagePatient()
        {

        }

        //public void CreatePatient(Patient patient,SmartCard smart)
        //{
        //    using (ePatientDB db = new ePatientDB())
        //    {
        //        //using (var transaction = new TransactionScope())
        //        //{
        //        try
        //        {
        //            // Create entity
        //            db.Patients.Add(patient);
        //            //db.SaveChanges();
        //            smart.PersonID = patient.PatientID;
        //            db.SmartCards.Add(smart);
        //            db.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //        //}
        //    }
        //}

        public bool CreatePatient(PatientInputModel patient)
        {
            bool success = false;
            using (ePatientDB db = new ePatientDB())
            {
                try
                {
                    Patient pt = new Patient();
                    SmartCard sc = new SmartCard();

                    pt.Title = patient.Title;
                    pt.Fname = patient.Fname;
                    pt.Lname = patient.Lname;
                    pt.Sex = patient.Sex;
                    pt.DOB = patient.DOB;
                    pt.Address = patient.Address;
                    pt.City = patient.City;
                    pt.Pincode = patient.Pincode;
                    pt.Phone = patient.Phone;
                    pt.Email = patient.Email;
                    pt.EnrollmentDate = patient.EnrollmentDate;

                    sc.CSN = patient.CSN;
                    sc.issueDate = patient.issueDate;
                    // Create entity
                    db.Patients.Add(pt);
                    //db.SaveChanges();
                    sc.PersonID = pt.PatientID;
                    db.SmartCards.Add(sc);

                    int result = db.SaveChanges();
                    if(result>0)
                    {
                        success = true; 
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return success;
        }


        public List<PatientSearchList> SearchPatient(string Fname, string Lname, string phone, double csn)
        {
            using (ePatientDB db = new ePatientDB())
            {
                IQueryable<PatientSearchList> _ps = null;
                _ps = (from pt in db.Patients
                       join sm in db.SmartCards on pt.PatientID equals sm.PersonID
                       where
                       (pt.Fname.ToUpper().Trim().Contains(Fname.ToUpper().Trim())) ||
                       (pt.Lname.ToUpper().Trim().Contains(Lname.ToUpper().Trim())) ||
                       (pt.Phone.ToUpper().Trim().Contains(phone.ToUpper().Trim()))
                       || (sm.CSN==csn)
                       orderby pt.PatientID descending
                       select new PatientSearchList()
                       {
                           PatientID = pt.PatientID,
                           Fname = pt.Fname,
                           Lname = pt.Lname,
                           Fullname = pt.Fname  +" "+ pt.Lname,
                           DOB = pt.DOB,
                           Phone = pt.Phone,
                           CSN = sm.CSN,
                           issueDate = sm.issueDate
                       });
                return _ps.ToList();
            }
        }

        public PatientInputModel getPatientInfoForEdit(int patientId)
        {
            PatientInputModel pt = new PatientInputModel();

            using (ePatientDB db = new ePatientDB())
            {
                Patient patient = db.Patients.Find(patientId);

                pt.PatientID = patient.PatientID;
                pt.Title = patient.Title;
                pt.Fname = patient.Fname;
                pt.Lname = patient.Lname;
                pt.Sex = patient.Sex;
                pt.DOB = patient.DOB;
                pt.Address = patient.Address;
                pt.City = patient.City;
                pt.Pincode = patient.Pincode;
                pt.Phone = patient.Phone;
                pt.Email = patient.Email;
                pt.EnrollmentDate = patient.EnrollmentDate;
                //pt.Allergies = patient.Allergies;
               // pt.Problems = patient.Problems;

                SmartCard card = db.SmartCards.Where(c => c.PersonID == patientId).FirstOrDefault();

                if(card!=null)
                {
                    pt.CSN = card.CSN;
                    pt.issueDate = card.issueDate;
                }
                
            }
            


            return pt;
        }

        public bool EditPatient(PatientInputModel patient)
        {
            bool sucess = false;
            using (ePatientDB db = new ePatientDB())
            {
                //using (var transaction = new TransactionScope())
                //{
                try
                {
                    Patient pt = new Patient();
                    SmartCard sc = new SmartCard();

                    pt.PatientID = patient.PatientID;
                    pt.Title = patient.Title;
                    pt.Fname = patient.Fname;
                    pt.Lname = patient.Lname;
                    pt.Sex = patient.Sex;
                    pt.DOB = patient.DOB;
                    pt.Address = patient.Address;
                    pt.City = patient.City;
                    pt.Pincode = patient.Pincode;
                    pt.Phone = patient.Phone;
                    pt.Email = patient.Email;
                    pt.EnrollmentDate = patient.EnrollmentDate;

                    sc.CSN = patient.CSN;
                    sc.issueDate = patient.issueDate;
                    sc.PersonID = pt.PatientID;


                    db.Entry(pt).State = EntityState.Modified;
                    db.SaveChanges();

                    SmartCard sc2 = db.SmartCards.Find(patient.PatientID);

                    if(sc2==null) //New add
                    {
                        db.SmartCards.Add(sc);
                    }
                    else
                    {
                        //db.Entry(sc).State = EntityState.Modified;
                        db.SmartCards.AddOrUpdate(s => s.PersonID, sc);
                    }
                    db.SaveChanges();
                    
                    sucess = true;
                    
                }
                catch (Exception)
                {
                    throw;
                }
                //}
            }
            return sucess;
        }


        //public void EditPatient(PatientInputModel patient)
        //{
        //    using (ePatientDB db = new ePatientDB())
        //    {
        //        //using (var transaction = new TransactionScope())
        //        //{
        //        try
        //        {
        //            Patient pt = db.Patients.Find(patient.PatientID);
        //            SmartCard sc = db.SmartCards.Find(patient.PatientID);
        //            //SmartCard sc = new SmartCard();

        //            pt.PatientID = patient.PatientID;
        //            pt.Title = patient.Title;
        //            pt.Fname = patient.Fname;
        //            pt.Lname = patient.Lname;
        //            pt.Sex = patient.Sex;
        //            pt.DOB = patient.DOB;
        //            pt.Address = patient.Address;
        //            pt.City = patient.City;
        //            pt.Pincode = patient.Pincode;
        //            pt.Phone = patient.Phone;
        //            pt.Email = patient.Email;
        //            pt.EnrollmentDate = patient.EnrollmentDate;

        //            sc.CSN = patient.CSN;
        //            sc.issueDate = patient.issueDate;
        //            sc.PersonID = pt.PatientID;

        //            db.Patients.AddOrUpdate(p=>p.PatientID,pt);

        //            db.SmartCards.AddOrUpdate(s => s.PersonID, sc);
        //            //db.Entry(pt).State = EntityState.Modified;
        //            //db.SaveChanges();
        //            //db.Entry(sc).State = EntityState.Modified;
        //            //db.SaveChanges();

        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //        //}
        //    }
        //}


        public PatientInputModel getSinglePatientDetails(int patientId)
        {
            PatientInputModel pt = new PatientInputModel();

            using (ePatientDB db = new ePatientDB())
            {
                Patient patient = db.Patients.Find(patientId);

                pt.PatientID = patient.PatientID;
                pt.Title = patient.Title;
                pt.Fname = patient.Fname;
                pt.Lname = patient.Lname;
                pt.Sex = patient.Sex;
                pt.DOB = patient.DOB;
                pt.Address = patient.Address;
                pt.City = patient.City;
                pt.Pincode = patient.Pincode;
                pt.Phone = patient.Phone;
                pt.Email = patient.Email;
                pt.EnrollmentDate = patient.EnrollmentDate;
                pt.Allergies = patient.Allergies;
                pt.Problems = patient.Problems;

                SmartCard card = db.SmartCards.Where(c => c.PersonID == patientId).FirstOrDefault();

                if (card != null)
                {
                    pt.CSN = card.CSN;
                    pt.issueDate = card.issueDate;
                }

            }



            return pt;
        }
    }
}