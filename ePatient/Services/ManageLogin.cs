﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePatient.DAL;
using ePatient.Models;

namespace ePatient.Services
{
    public class ManageLogin:SingletonHelper<ManageLogin>
    {
        //private ePatientDB db = new ePatientDB();

        private ManageLogin()
        {

        }

        /// <summary>
        /// Verify input data from LogIn
        /// </summary>
        /// <param name="userLogin"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool VerifyLogin(string userLogin, string password)
        {
            bool result = false;
            
            using (ePatientDB db = new ePatientDB())
            {
                try
                {
                    UserMaster login = db.Users.Where(log => log.UserName == userLogin).FirstOrDefault();
                    if (login != null)
                    {
                        if (password.Equals(login.Password) && login.IsActive == true)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {

                }
           }

            return result;
        }
    }
}