﻿var patientVM = {
    ID: ko.observable(),
    Title: ko.observable(),
    Fname: ko.observable(),
    Lname: ko.observable(),
    Sex: ko.observable(),
    DOB: ko.observable(),
    Address: ko.observable(),
    City: ko.observable(),
    Pincode: ko.observable(),
    Phone: ko.observable(),
    Email: ko.observable(),
    EnrollmentDate: ko.observable(),
    CSN: ko.observable(),
    issueDate:ko.observable(),

    //Load Patient Info
    LoadPatientData: function () {
        var pat_id = (document.URL.substring(document.URL.lastIndexOf("/") + 1, document.URL.length));
        $.ajax({
            url: '/ePatient/GetPatientInfo',
            type: 'get',
            dataType: 'json',
            data: { id: pat_id }
            
        }).done(function (data) {
            
            $(data).each(function (index, element) {
               
                element.ID = pat_id;
                
                patientVM.ID(element.ID);
                patientVM.Title(element.Title);
                patientVM.Fname(element.Fname);
                patientVM.Lname(element.Lname);
                alert(element.Lname);
            });//each loop ends-----

        })/*done ends---*/.error(function (ex) {
            alert("Error");
        });//error ends----

    },

    //Save Patient
    btnSavePatient: function () {

        $.ajax({
            url: '/ePatient/ProcessForm',
            type: 'post',
            dataType: 'json',
            data: ko.toJSON(this),
            contentType: 'application/json',
            success: function (result) {
                window.location.href = urlPath + '/';
            },
            error: function (err) {
                if (err.responseText == "success")
                { window.location.href = urlPath + '/'; }
                else {
                    alert(err.responseText);
                }
            },
            complete: function () {
            }
        });

    }
};
ko.applyBindings(patientVM);

$(document).ready(function () {
    $("#divProgress").css("display", "none");
    patientVM.LoadPatientData();
});

//$(document).ready(function () {
   
//    //initializing viewmodel
//  // patientmodel = new viewModel();
//    //binding for ko
//   // ko.applyBindings(patientmodel);
//    fetchPatientData();
//});


//var viewmodel = {
//    patientCollection: ko.observableArray()
    
//};

//function fetchPatientData() {
   
//    // Ajax call for getting patient data
//    var pat_id = (document.URL.substring(document.URL.lastIndexOf("/") + 1, document.URL.length));
//    $("#save_msg").hide();
//    $.ajax({
//        type: "GET",
//        url: "/ePatient/GetPatientInfo",
//        data: { id: pat_id }
//    }).done(function (data) {
        
//        $(data).each(function (index, element) {
//            //var milli = element.dob.replace(/\/Date\((-?\d+)\)\//, '$1');
//            //var d = new Date(parseInt(milli)).toLocaleDateString("en-GB");

//            //element.dob = d;
//            element.PatientID = pat_id;
//            //var selectSex = Number(element.Sex);
            
//            //viewModel.selectedGender(selectSex);
//           // element.Sex = getGenderText(viewModel.genderOptions(), selectSex);

//            // element.Sex = (element.Sex == '1') ? "Male" : "Female";
//            //alert("pushing into collection");
//            var mappedItems = {
//                PatientID: ko.observable(element.PatientID),
//                Titles: ko.observable(element.Title),
//                Fname: ko.observable(element.Fname)
//                //Sex: ko.observable(element.Sex)
//            };
//            viewmodel.patientCollection(mappedItems);
            
//      });


//    }).error(function (ex) {
//        alert("Error");
//    });
//    ko.applyBindings(viewmodel);
//}

//var initialData = '@Html.Raw(ViewBag.InitialData)'; //get the raw json
//var parsedJSON = $.parseJSON(initialData); //parse the json client side
//var patientmodel = {
//    Patients: ko.observableArray([]),
//    PatientID: ko.observable(parsedJSON.PatientID),
//    Title: ko.observable(parsedJSON.Title),
//    Fname: ko.observable(parsedJSON.Fname),
   
//    SavePatient: function () {
//        $.ajax({
//            url: '/ePatient/Edit',
//            type: 'post',
//            dataType: 'json',
//            data: ko.toJSON(this),
//            contentType: 'application/json',
//            success: function (result) {
//            },
//            error: function (err) {
//                if (err.responseText == "Creation Failed")
//                { window.location.href = '/ePatient/Index/'; }
//                else {
//                    alert("Status:" + err.responseText);
//                    window.location.href = '/ePatient/Index/';;
//                }
//            },
//            complete: function () {
//                window.location.href = '/ePatient/Index/';
//            }
//        });
//    }
//};







//$(document).ready(function () {


//    // Here's my data model
//    var ViewModel = function () {
//        this.Id = ko.observable();
//        this.Title = ko.observable();
//        this.Fname = ko.observable();
//        this.Lname = ko.observable();
//        //this.name = ko.observable();
        

//        this.save = function () {

//            var jsonData = ko.toJSON(this);
//           // alert("Could now send this to server: " + JSON.stringify(jsonData));

//            $.ajax({
//                url: '/ePatient/ProcessForm',
//                dataType: 'json',
//                type: 'POST',
//                data: { patient: jsonData },

//                success: function (data) {
//                    alert("Successful");
//                },
//                failure: function () {
//                    alert("Unsuccessful");
//                }
//            });
//        }
//    };


//    ko.applyBindings(new ViewModel());

//});