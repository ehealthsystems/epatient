﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ePatient.Models
{
    public class PatientSearchList
    {
        public int PatientID { get; set; }
        public String Fname { get; set; }
        public String Lname { get; set; }
        public String Phone { get; set; }
        public String Fullname { get; set; }

        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }

        public Double CSN { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime issueDate { get; set; }
    }

    public class PatientSearchModel
    {
        List<PatientSearchList> list { get; set; }
    }
}