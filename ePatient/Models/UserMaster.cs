﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ePatient.Models
{
    public class UserMaster
    {
        [Key]
        public int UserID { get; set; }

        public int FacilityID { get; set; }
        
        
        public String Title { get; set; }
        
        [Display(Name = "First-Mid Name")]
        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters.")]
        public String FirstName { get; set; }

        [Display(Name = "Last Name")]
        [StringLength(50, ErrorMessage = "Last name cannot be longer than 50 characters.")]
        public String LastName { get; set; }

        //[Display(Name = "Username")]
        [Display(Name = "Username", ResourceType = typeof(Resources.Resources))]
        [Column("LoginID")]
        [StringLength(100, ErrorMessage = "Login ID cannot be longer than 100 characters.")]
        public String UserName { get; set; }

       // [Display(Name = "Password")]
        [Display(Name = "Password", ResourceType = typeof(Resources.Resources))]
        [StringLength(40, ErrorMessage = "Password length should be between 5 and 40 characters.",MinimumLength=5)]
        public String Password { get; set; }

        public bool? IsAdmin { get; set; }

        public bool? IsDoctor { get; set; }

        public bool? IsActive { get; set; }

        public DateTime CreateDate { get; set; }

        public virtual Facility Facility { get; set; }

     }
}