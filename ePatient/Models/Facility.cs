﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ePatient.Models
{
    public class Facility
    {
        [Key]
        public int FacilityID       { get; set; }

        public String FacilityName  { get; set; }

        public virtual ICollection<UserMaster> Users { get; set; }


    }
}