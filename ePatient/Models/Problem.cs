﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;


namespace ePatient.Models
{
    
    public enum Organ
    {
        Heart,Eye,Ear,Hands,Legs,Lungs,Throat,Liver,Blood,Others

    }
    public class Problem
    {
        [Key]
        public int ProblemID             { get; set; }
        public int PatientID             { get; set; }
        public Organ Organs              { get; set; }
       
        [Display(Name="Problem Details")]
        [StringLength(500, ErrorMessage="Patient problem details should not exceed 500 chars")]
        public String Details            { get; set; }

        [Display(Name = "Date when reported")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ReportedOn       { get; set; }

        /*
        [Display(Name = "Date when detected")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DetectedOn        { get; set; }
        */
        public virtual Patient Patient { get; set; }

        /* Added by Raj[30.07.2014] for Organ Dropdown */
        public Problem()
        {
            OrganList = new List<SelectListItem>();
        }
        [Display(Name="Organs")]
        public int OrganId { get; set; }
        public IEnumerable<SelectListItem> OrganList { get; set; }
        /* Add ends */
    }
}