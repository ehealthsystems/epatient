﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ePatient.Models
{
    public class SmartCard
    {
        [Key]
        [ForeignKey("Person")]
        public int PersonID { get; set; }

        [Display(Name = "Card Serial Number")]
        public Double CSN { get; set; }

        [Display(Name="Card Issue Date")]
        [DataType(DataType.DateTime)]
        public DateTime issueDate { get; set; }

        // define issuer details
        // define additional card security attributes , secret key etc.

        public virtual Patient Person { get; set; }

    }


}