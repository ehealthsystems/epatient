﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ePatient.Models
{
    public class PatientInputModel0
    {
        public String Fname { get; set; }        
        public String Lname { get; set; }
    }
    public class PatientInputModel
    {
        public int PatientID { get; set; }

        [Required]
        public String Title { get; set; }

        [Display(Name = "First Name")]
        public String Fname { get; set; }

        [Display(Name = "Last Name")]
        public String Lname { get; set; }

        [Display(Name = "Date of Birth")]
        public DateTime DOB { get; set; }

        [Display(Name = "Gender")]
        public Gender Sex { get; set; }


        public String Address { get; set; }
        public String City { get; set; }
        public String Pincode { get; set; }
        public String Phone { get; set; }
        public String Email { get; set; }

        [Display(Name = "Enrollment Date")]
        public DateTime EnrollmentDate { get; set; }

        public String FullName
        {
            get
            {
                return this.Lname + " " + this.Fname;
            }

        }

        public virtual ICollection<Allergy> Allergies { get; set; }

        public virtual ICollection<Problem> Problems { get; set; }

        [Display(Name = "Card Serial Number")]
        public Double CSN { get; set; }

        [Display(Name = "Card Issue Date")]
        public DateTime issueDate { get; set; }

    }
}