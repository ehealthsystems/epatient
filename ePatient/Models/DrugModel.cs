﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ePatient.Models
{
    public class DrugModel
    {
        [Key]
        int DrugModelId { get; set; }
        string drugDescr{ get; set; }
        string drugStrengthTxt { get; set; }
        public virtual ICollection<DrugSignature> Signatures { get; set; }
    }

    public class DrugSignature
    {
        int drugsignatureId { get; set; }
        int drugId { get; set; }
        string descr { get; set; }
    }
}