﻿using System;
using System.Collections.Generic;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ePatient.Models
{
    public class Allergy
    {
        [Key]
        public int AllergyID { get; set; }
        public int PatientID { get; set; }

        [Display(Name = "Allergy Details")]
        [StringLength(500, ErrorMessage = "Patient allergy details length should not exceed 500 chars")]
        public String Details { get; set; }

        [Display(Name = "Date when reported")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ReportedOn { get; set; }

        [Display(Name = "Date when detected")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DetectedOn { get; set; }

        public virtual Patient Patient { get; set; }
    }

   
}