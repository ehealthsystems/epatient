﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace ePatient.Models
{

    public class Address
    {
        public String Street    { get; set; }
        public String City      { get; set; }
        public String PostCode  { get; set; }
        public String Country   { get; set; }
    }
    public class Contact
    {
        public String HomePhone { get; set; }
        public String WorkPhone { get; set; }
        public String Email     { get; set; }
    }

    public enum Gender
    {
        Male,Female
    }


    public class Patient
    {
        [Key]
        public int PatientID           { get; set; }
        public String Title     { get; set; }
        //public Titles Title { get; set; }
        
        [Display( Name = "First Name")]
        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters.",MinimumLength=1)]
        public String Fname     { get; set; }

        [StringLength(50)]
        [Display(Name = "Last Name")]
        public String Lname { get; set; }
        
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DOB     { get; set; }
   
        public Gender Sex        { get; set; }
        public String Address    { get; set; }
        public String City       { get; set; }
        public String Pincode    { get; set; }
        public String Phone      { get; set; }
        public String Email      { get; set; }

        [Display(Name = "Date of Enrollment")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EnrollmentDate { get; set; }
        
        [Display(Name = "Full Name")]
        public String FullName
        {
            get
            {
                return this.Lname + " " + this.Fname;
            }
           
        }

        public virtual ICollection<Allergy> Allergies { get; set; }

        public virtual ICollection<Problem> Problems { get; set; }

        public virtual SmartCard SmartCard { get; set; }

        /*Added by Raj */
        //public enum Titles
        //{
        //    Mr,Mrs,Ms,Prof,Doc
        //}
    }

 
}
