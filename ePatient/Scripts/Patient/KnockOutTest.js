﻿viewModel = {
    patientCollection: ko.observableArray()
};

$(document).ready(function () {
    
    $.ajax({
        type: "GET",
        url: "/ePatient/PatientSearch",
    }).done(function (data) {
        $(data).each(function (index, element) {
            var mappedItem =
                {
                    Fname: ko.observable("Alex"),
                    Lname: ko.observable(element.Lname),
                    Mode: ko.observable("display")
                };
            viewModel.patientCollection.push(mappedItem);
        });
        ko.applyBindings(viewModel);
    }).error(function (ex) {
        alert("Error");
    });

    $(document).on("click", ".kout-edit", null, function (ev) {
        alert("1");
        var current = ko.dataFor(this);
        current.Mode("search");
    });

    $(document).on("click", ".kout-update", null, function (ev) {
        alert("2");
        var current = ko.dataFor(this);
        alert(current.Fname);
        searchData(current);

        current.Mode("display");
    });

    function searchData(currentData) {
        var postUrl = "ePatient/PatientSearch";
        var submitData = {
            Fname: currentData.Fname(),
            Lname: currentData.Lname()//,
            //Value: currentData.Value()

        };
                
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: postUrl,
            data: JSON.stringify(submitData)
        }).done(function (id) {
            currentData.Id(id);
        }).error(function (ex) {
            alert("ERROR Saving");
        })

        
    }


});