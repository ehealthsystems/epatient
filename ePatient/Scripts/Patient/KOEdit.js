﻿$(document).ready(function () {
    var viewModel = new PatientViewModel();
    ko.applyBindings(viewModel);
});






function PatientViewModel() {
    var self = this;
    //Declare observable which will be bind with UI 
    self.PatientID = ko.observable("");
    self.Title = ko.observable("");
    self.Fname = ko.observable("");
    self.Class = ko.observable("");

    var Patient = {
        PatientID: self.PatientID,
        Title: self.Title,
        Fname: self.Fname,
        
    };
    self.Patient = ko.observable();
    self.Patients = ko.observableArray();

    self.edit = function (Patient) {
        self.Patient(Patient);

    }

    // Update product details
    self.change = function () {
        var Patient = self.Patient();

        $.ajax({
            url: "/ePatient/Edit",
            cache: false,
            type: 'PUT',
            contentType: 'application/json; charset=utf-8',
            data: ko.toJSON(Patient),
            success: function (data) {
                self.Patients.removeAll();
                self.Patients(data); //Put the response in ObservableArray
                self.Patient(null);
                alert("Record Updated Successfully");

            }
        })
    .fail(
    function (xhr, textStatus, err) {
        alert(err);
    });
    }


}