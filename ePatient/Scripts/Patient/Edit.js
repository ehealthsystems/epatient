﻿
function patientVM() {
    var self=this;
    self.PatientID= ko.observable();
    self.Title= ko.observable();
    self.Fname= ko.observable();
    self.Lname = ko.observable();

    self.Sex = ko.observable();
    //self.M = ko.computed(
    //    {
    //        read: function () {
    //            return this.Sex() == "M";
    //        },
    //        write: function (value) {
    //            if (value)
    //                this.Sex("M");
    //        }
    //    }
    //, this);
    //self.F = ko.computed(
    //    {
    //        read: function () {
    //            return this.Sex() == "F";
    //        },
    //        write: function (value) {
    //            if (value)
    //                this.Sex("F");
    //        }
    //    }
    //, this);

    self.DOB = ko.observable();
    self.Address = ko.observable('');
    self.City = ko.observable();
    self.Pincode = ko.observable();
    self.Phone = ko.observable();
    self.Email = ko.observable();
    self.EnrollmentDate = ko.observable();
    self.CSN = ko.observable();
    self.issueDate = ko.observable();

    self.charactersRemaining = ko.computed(function () {
        return 120 - self.Address().length;
    });

    //Load Patient Info
    self.LoadPatientData = function () {
        var pat_id = (document.URL.substring(document.URL.lastIndexOf("/") + 1, document.URL.length));
        $.ajax({
            url: '/ePatient/GetPatientInfo',
            type: 'get',
            dataType: 'json',
            data: { id: pat_id }

        }).done(function (data) {

            $(data).each(function (index, element) {

                element.PatientID = pat_id;

               var milli = element.DOB.replace(/\/Date\((-?\d+)\)\//, '$1');
               var d = new Date(parseInt(milli)).toLocaleDateString("en-GB");

                element.dob = d;

                self.PatientID(element.PatientID);
                self.Title(element.Title);
                self.Fname(element.Fname);
                self.Lname(element.Lname);
                self.Address(element.Address);
                self.Sex(element.Sex);

                if (element.Sex == 0)
                    self.Sex('0');
                else
                    self.Sex('1');
               //self.Sex(element.Sex);
               //alert(element.dob);

                self.DOB(element.dob);


            });//each loop ends-----

        })/*done ends---*/.error(function (ex) {
            alert("Error");
        });//error ends----

    };

    //Save Patient
    self.btnSavePatient = function () {
        $("#divProgress").css("display", "block");
        $.ajax({
            url: '/ePatient/ProcessForm',
            type: 'post',
            dataType: 'json',
            data: ko.toJSON(this),
            contentType: 'application/json',
            success: function (result) {
                $('#myModal').modal('show');
                if (result.context == "Edit") {
                    if (result.success == "1")
                        $('#divMsg').html("<span class='label label-success'>Patient modified successfully!</span>");
                    else
                        $('#divMsg').html("<span class='label label-danger'>Error! Please try again later</span>");
                }
                if (result.context == "Add") {
                    if (result.success == "1")
                        $('#divMsg').html("<span class='label label-success'>Patient added successfully!</span>");
                    else
                        $('#divMsg').html("<span class='label label-danger'>Error! Please try again later</span>");
                }
                $("#divProgress").css("display", "none");
            },
            error: function (err) {
                if (err.responseText == "success")
                { window.location.href = urlPath + '/'; }
                else {
                    alert(err.responseText);
                }
            },
            complete: function () {
            }
        });

    };
    self.LoadPatientData();
};
ko.applyBindings(new patientVM());

$(document).ready(function () {
    $("#divProgress").css("display", "none");
});
