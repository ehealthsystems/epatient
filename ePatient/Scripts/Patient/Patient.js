﻿function SavePatient() {
    var form = $('#patientForm');
    if (form.valid()) {
        $("#divProgress").css("display", "block");
        $.ajax({
            url: '/ePatient/ProcessForm',
            //cache: false,
            data: form.serialize(),
            type: 'POST',
            success: function (result) {
                //$('#hdnResult').val(result.id);
                if (result.context == "Edit") {
                    if (result.success == "1")
                        $('#divMsg').html("<span class='label label-success'>Patient modified successfully!</span>");
                    else
                        $('#divMsg').html("<span class='label label-danger'>Error! Please try again later</span>");
                }
                if (result.context == "Add") {
                    if (result.success == "1")
                        $('#divMsg').html("<span class='label label-success'>Patient added successfully!</span>");
                    else
                        $('#divMsg').html("<span class='label label-danger'>Error! Please try again later</span>");
                }
                //$('#divMsg').html(result.message+result.id);
                $("#divProgress").css("display", "none");
            },
            error: function () {
                $('#divMsg').html("<span class='label label-danger'>Error! Please try again later</span>");
            }
        });
    }
    
}