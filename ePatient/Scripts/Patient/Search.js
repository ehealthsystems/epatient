﻿var itemsPerPage = 20;

function initSearchList() {

    jQuery("#tableSearch").jqGrid({
        url: '/ePatient/PatientSearch',
        cache: false,
        postData: {
            Fname: function () { return $("#txtFname").val() },
            Lname: function () { return $("#txtLname").val() },
            Phone: function () { return $("#txtPhone").val() },
            CSN:   function () { return $("#txtCSN").val() }
        },
        datatype: "json",
        mtype: 'GET',
        colNames: ['', 'First Name', 'Last Name', 'DOB', 'CSN', 'Issue Date'],
        colModel: [
				{ name: 'PatientID', key: true, index: 'PatientID', hidden: true, sortable: false },
                { name: 'Fname', index: 'Fname', width: 70, sortable: false },
                { name: 'Lname', index: 'Lname',  width: 70, sortable: false },
                { name: 'DOB', index: 'DOB', width: 70, sortable: false },
                { name: 'CSN', index: 'CSN', width: 70, sortable: false },
                { name: 'issueDate', index: 'issueDate', width: 70, sortable: false },
        ],
        rowNum: itemsPerPage,
        autowidth: true,
        shrinkToFit: true,
        rowList: [10, 20, 30],
        sortname: 'PatientID',
        sortorder: 'desc',
        altRows: true,
        viewrecords: true,
        jsonReader: {
            repeatitems: false
        },
        height: '100%',
        gridComplete: function () {
            var recs = parseInt($("#tableSearch").getGridParam("records"), 10);
            if (isNaN(recs) || recs == 0)
                $("#tableSearch").hide();
            else
                $('#tableSearch').show();
        }
    });
}

function refreshGrid() {
    //alert("1234");
    $("#tableSearch").trigger("reloadGrid", [{ page: 1 }]);
}