﻿/* ePatientDB.cs */

using ePatient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ePatient.DAL;

namespace ePatient.DAL
{
    /// <summary>
    /// Convention class for convertion the SQL server datetime to datetime2 format
    /// </summary>
    public class DateTime2Convention : Convention
    {
        public DateTime2Convention()
        {
            this.Properties<DateTime>()
                .Configure(c => c.HasColumnType("datetime2"));
        }
    }

    /// <summary>
    /// The database cobntext class for the application ePatient 
    /// </summary>
    public class ePatientDB : DbContext
    {

        public ePatientDB() : base("ePatientDB")
        {
           Database.SetInitializer<ePatientDB>(new DropCreateDatabaseIfModelChanges<ePatientDB>());
           // Database.SetInitializer<ePatientDB>(new DropCreateDatabaseAlways<ePatientDB>());
        }
        public DbSet<Patient> Patients          { get; set; }
        public DbSet<SmartCard> SmartCards      { get; set; }
        public DbSet<Allergy> Allergies         { get; set; }
        public DbSet<Problem> Problems          { get; set; }
        public DbSet<UserMaster> Users          { get; set; }
        public DbSet<Facility> Facilities       { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Setting to drop the Plurarizing naming convention
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // Setting to change the SQL server default datetime property to datetime2
            modelBuilder.Conventions.Add(new DateTime2Convention());
            
            //modelBuilder.Properties<DateTime>()
            //.Configure(c => c.HasColumnType("datetime2"));
            
            // Setting for defining the relationship between Patient - Problem classes
            modelBuilder.Entity<Patient>()
                .HasMany(a => a.Problems)
                .WithRequired(a => a.Patient);

            // Setting for defining the relationship between Patient - Allergy classes
            modelBuilder.Entity<Patient>()
                .HasMany(a => a.Allergies)
                .WithRequired(a => a.Patient);
            
            // Setting for defining the relationship between Usermaster - Facility classes
            modelBuilder.Entity<UserMaster>()
                .HasRequired(a => a.Facility);

            // Setting for defining the relationship between Patient - SmartCard classes
            modelBuilder.Entity<Patient>()
                .HasRequired(a => a.SmartCard);
        }

    }

}