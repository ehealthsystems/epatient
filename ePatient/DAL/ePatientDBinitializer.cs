﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePatient.Models;
using System.Data.Entity;
using ePatient.DAL;
using System.Globalization;

namespace ePatient.DAL
{
    public class ePatientDBinitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ePatientDB>
    //public class ePatientDBinitializer : System.Data.Entity.DropCreateDatabaseAlways<ePatientDB>   
    {
        public DateTime strToDate(String strDate)
        {

            IFormatProvider culture = new CultureInfo("en-GB", true);

            return DateTime.ParseExact(strDate, "dd-MM-yyyy", culture);

        }
        protected override void Seed(ePatientDB context)
        {
            //string timeString = "11/12/2009 13:30:00.000";
            IFormatProvider culture = new CultureInfo("en-GB", true); 
            //DateTime dateVal = strToDate(Exact(timeString, "dd/MM/yyyy HH:mm:ss.fff", culture);
            
            var patients = new List<Patient>
            {
                new Patient{PatientID= 1,Title="Mr",Fname="Alex",Lname="Mathews",Sex=Gender.Male,DOB=strToDate("22-01-1980"), Address="101 wood Street",City="NH",Pincode="12113",Phone="+49 52635721", Email="alex.mat@gmail.com" },
                new Patient{PatientID= 2,Title="Mr",Fname="Ted",Lname="Hoff",Sex=Gender.Male,DOB=strToDate("12-10-1970"), Address="12A Northern Lane",City="PY",Pincode="4424",Phone="28375278536", Email="ted.hoff@gmail.com" }
                /*
                new Patient{PatientID= 3,Title="Mr",Fname="Jack",Lname="Marshall",Sex=Gender.Male,DOB=strToDate("03-12-1988") } ,
                new Patient{PatientID= 4,Title="Mrs",Fname="Lousie",Lname="Hops",Sex=Gender.Female,DOB=strToDate("13-04-1990"), Address=address2, Contact=contact2} ,
                new Patient{PatientID= 5,Title="Mrs",Fname="Tina",Lname="Sarah",Sex=Gender.Female,DOB=strToDate("12-01-1966") } ,
                new Patient{PatientID= 6,Title="Mr",Fname="Captain",Lname="America",Sex=Gender.Male,DOB=strToDate("16-09-1955") }
                 */
            };

            patients.ForEach(s=> context.Patients.Add(s));
            context.SaveChanges();


            var problems = new List<Problem>
            {
                new Problem{PatientID= 1,Organs=Organ.Ear,Details="The Right Ear has severe loss, 80 dB", ReportedOn=strToDate("14-10-2000")},
                new Problem{PatientID= 1,Organs=Organ.Others,Details="Vomitting tendency after dinner", ReportedOn=strToDate("23-01-2009")},
                new Problem{PatientID= 1,Organs=Organ.Hands,Details="The Left Arm is often felt numb in the morning", ReportedOn=strToDate("18-03-2013")},
                new Problem{PatientID= 2,Organs=Organ.Heart,Details="The pule rate is fluctuating in the morning", ReportedOn=strToDate("01-01-2006")},
                new Problem{PatientID= 2,Organs=Organ.Blood,Details="The blood does not clot after a cut", ReportedOn=strToDate("30-03-2014")}
               /*
                new Problem{PatientID= 3,Organs=Organ.Others,Details="The snoring at  night accompanied by chest pain", ReportedOn=strToDate("18-03-2013")},
                new Problem{PatientID= 4,Organs=Organ.Liver,Details="Pancreatic pain occassional", ReportedOn=strToDate("01-01-2006")},
                new Problem{PatientID= 4,Organs=Organ.Others,Details="Pain in lower abdomen", ReportedOn=strToDate("30-03-2014")},
                new Problem{PatientID= 5,Organs=Organ.Others,Details="Frequent anger accompanied by headache/migrain", ReportedOn=strToDate("18-03-2013")},
                new Problem{PatientID= 5,Organs=Organ.Hands,Details="The hand shakes after taking pressure capsules", ReportedOn=strToDate("01-01-2006")},
                new Problem{PatientID= 6,Organs=Organ.Lungs,Details="Severe Bronchitis accompanied by blood in coughing", ReportedOn=strToDate("30-03-2014")},
               */
            };

            problems.ForEach(s => context.Problems.Add(s));
            context.SaveChanges();

            var facilities = new List<Facility>
            {
                new Facility{FacilityName="Red Cross Clinic"},
                new Facility{FacilityName="Evergreen Clinic"}
                
            };

            facilities.ForEach(s => context.Facilities.Add(s));
            context.SaveChanges();

            var users = new List<UserMaster>
            {
                new UserMaster{FacilityID=1,FirstName="Roger",LastName="Miller",UserName="roger.miller",Password="admin123",IsAdmin=true,IsActive=true,IsDoctor=false},
                new UserMaster{FacilityID=1,Title="Dr.", FirstName="Richard",LastName="Right",UserName="richard.right",Password="doctor123",IsAdmin=false,IsActive=true,IsDoctor=true}
            };

            users.ForEach(s => context.Users.Add(s));
            context.SaveChanges();



        }
    }
}