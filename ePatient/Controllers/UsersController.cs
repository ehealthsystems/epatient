﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePatient.DAL;
using ePatient.Models;

namespace ePatient.Controllers
{
    public class UsersController : BaseController
    {
        private ePatientDB db = new ePatientDB();

        // GET: Users
        public async Task<ActionResult> Index()
        {
            if (Convert.ToString(Session["ISLOGGEDIN"]) != "True")
            {
                return RedirectToAction("LogOn", "UserAccount", null);
            }

            else
            {
                var users = db.Users.Include(u => u.Facility);
                return View(await users.ToListAsync());
            }
                
            
        }

        // GET: Users/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserMaster userMaster = await db.Users.FindAsync(id);
            if (userMaster == null)
            {
                return HttpNotFound();
            }
            return View(userMaster);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.FacilityID = new SelectList(db.Facilities, "FacilityID", "FacilityName");
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "UserID,FacilityID,Title,FirstName,LastName,UserName,Password,IsAdmin,IsDoctor,IsActive,CreateDate")] UserMaster userMaster)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(userMaster);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.FacilityID = new SelectList(db.Facilities, "FacilityID", "FacilityName", userMaster.FacilityID);
            return View(userMaster);
        }

        // GET: Users/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserMaster userMaster = await db.Users.FindAsync(id);
            if (userMaster == null)
            {
                return HttpNotFound();
            }
            ViewBag.FacilityID = new SelectList(db.Facilities, "FacilityID", "FacilityName", userMaster.FacilityID);
            return View(userMaster);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserID,FacilityID,Title,FirstName,LastName,UserName,Password,IsAdmin,IsDoctor,IsActive,CreateDate")] UserMaster userMaster)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userMaster).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.FacilityID = new SelectList(db.Facilities, "FacilityID", "FacilityName", userMaster.FacilityID);
            return View(userMaster);
        }

        // GET: Users/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserMaster userMaster = await db.Users.FindAsync(id);
            if (userMaster == null)
            {
                return HttpNotFound();
            }
            return View(userMaster);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            UserMaster userMaster = await db.Users.FindAsync(id);
            db.Users.Remove(userMaster);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
