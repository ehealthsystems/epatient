﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePatient.DAL;
using ePatient.Models;

namespace ePatient.Controllers
{
    public class AllergiesController : BaseController
    {
        private ePatientDB db = new ePatientDB();

        // GET: Allergies
        public ActionResult Index()
        {
            var allergies = db.Allergies.Include(a => a.Patient);
            return View(allergies.ToList());
        }

        // GET: Allergies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Allergy allergy = db.Allergies.Find(id);
            if (allergy == null)
            {
                return HttpNotFound();
            }
            return View(allergy);
        }

        // GET: Allergies/Create/patientid
        public ActionResult Create(int? id)
        {
            //var x = Convert.ToInt32("Hello"); //For Testing Exception Handling
            if (id != null)
            {
                Patient patient = db.Patients.Find(id);
                Allergy allergy = new Allergy();
                allergy.PatientID = id.Value;
                allergy.Patient = patient;

                return View(allergy);
            }
            else {
                return HttpNotFound();
            }
        }

         // POST: Allergies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AllergyID,PatientID,Details,ReportedOn,DetectedOn")] Allergy allergy)
        {
            if (ModelState.IsValid)
            {
                db.Allergies.Add(allergy);
                db.SaveChanges();
                return RedirectToAction("Edit", "ePatient", new { id = allergy.PatientID });
            }
            Patient patient = db.Patients.Find(allergy.PatientID);
            allergy.Patient = patient;
            return View(allergy);
        }

        // GET: Allergies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Allergy allergy = db.Allergies.Find(id);
            if (allergy == null)
            {
                return HttpNotFound();
            }
           // ViewBag.PatientID = new SelectList(db.Patients, "PatientID", "Title", allergy.PatientID);
            ViewBag.PatientID = allergy.PatientID;

            Patient patient = db.Patients.Find(allergy.PatientID);
            allergy.Patient = patient;

            return View(allergy);
        }

        // POST: Allergies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AllergyID,PatientID,Details,ReportedOn,DetectedOn")] Allergy allergy)
        {
            if (ModelState.IsValid)
            {
                db.Entry(allergy).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", "ePatient", new { id = allergy.PatientID });
            }
            ViewBag.PatientID = new SelectList(db.Patients, "PatientID", "Title", allergy.PatientID);
            return View(allergy);
        }

        // GET: Allergies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Allergy allergy = db.Allergies.Find(id);
            if (allergy == null)
            {
                return HttpNotFound();
            }
            return View(allergy);
        }

        // POST: Allergies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Allergy allergy = db.Allergies.Find(id);
            db.Allergies.Remove(allergy);
            db.SaveChanges();
            return RedirectToAction("Index", "ePatient");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
