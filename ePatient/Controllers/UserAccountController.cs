﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ePatient.Services;
using ePatient.Models;
using System.Web.Security;

namespace ePatient.Controllers
{
    public class UserAccountController : BaseController
    {
        // GET: UserAccount
        public ActionResult LogOn()
        {
            return View();
        }

        //[HttpPost]
        //public ActionResult LogOn(UserMaster model, string returnUrl)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        bool loginOk = true;
        //        loginOk = ManageLogin.Instance.VerifyLogin(model.UserName, model.Password);

        //        if (!loginOk)
        //        {
        //            Session["ISLOGGEDIN"] = "False";
        //            ModelState.AddModelError("", "The user name or password provided is incorrect.");
        //        }
        //        else
        //        {
        //            Session["ISLOGGEDIN"] = "True";
        //            return RedirectToAction("Index", "ePatient");
        //        }


        //    }

        //    return View(model);
        //}

        public JsonResult ProcessLoginRequest(UserMaster model)
        {

            bool loginOk = true;
            loginOk = ManageLogin.Instance.VerifyLogin(model.UserName, model.Password);

            if (!loginOk)
            {
                Session["ISLOGGEDIN"] = "False";
                return Json(new { success = "0" });
            }
            else
            {
                Session["ISLOGGEDIN"] = "True";
                return Json(new { success = "1" });
            }

        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            return RedirectToAction("LogOn", "UserAccount");
        }

        public ActionResult SetCulture(string culture)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);
            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture;   // update cookie value
            else
            {
                cookie = new HttpCookie("_culture");
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            // return RedirectToAction("Index");
            return RedirectToAction("LogOn","UserAccount");
        }  
    }
}