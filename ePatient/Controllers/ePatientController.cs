﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePatient.Models;
using ePatient.DAL;
using ePatient.Services;
using System.Threading;
using System.Web.Helpers;
using System.Web.Script.Serialization;

namespace ePatient.Controllers
{
    public class ePatientController : BaseController
    {
        private ePatientDB db = new ePatientDB();

        // GET: /ePatient/
        public ActionResult Index()
        {
            if (Convert.ToString(Session["ISLOGGEDIN"]) != "True")
            {
                return RedirectToAction("LogOn", "UserAccount", null);
            }
               
            else
                return View(db.Patients.ToList());
        }

                // GET: /ePatient/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PatientInputModel patientinfo = ManagePatient.Instance.getSinglePatientDetails(id.Value);
            if (patientinfo == null)
            {
                return HttpNotFound();
            }
            return View(patientinfo);
        }

        // GET: /ePatient/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /ePatient/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include="PatientID,Title,Fname,Lname,DOB,Sex,EnrollmentDate,CSN,issueDate")] Patient patient)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        SmartCard _smart = new SmartCard();
        //        _smart.PersonID = patient.PatientID;
        //        _smart.issueDate = patient.SmartCard.issueDate;
        //        _smart.CSN = patient.SmartCard.CSN;

        //        ManagePatient.Instance.CreatePatient(patient,_smart);
        //        //db.Patients.Add(patient);
        //        //db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(patient);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PatientInputModel patient)
        {
            if (ModelState.IsValid)
            {
                 ManagePatient.Instance.CreatePatient(patient);
                //db.Patients.Add(patient);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(patient);
        }

        // GET: /ePatient/Edit/5
        public ActionResult Edit(int? id)
        {
           // if (id == null)
           // {
           //     return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
           // }
            
           // PatientInputModel patientinfo = ManagePatient.Instance.getPatientInfoForEdit(id.Value);
           // if (patientinfo == null)
           // {
           //     return HttpNotFound();
           // }
           //return View(patientinfo);
            return View();
        }

        public ActionResult GetPatientInfo(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Patient patient = db.Patients.Find(id);

            PatientInputModel patientinfo = ManagePatient.Instance.getPatientInfoForEdit(id.Value);
            // JavaScriptSerializer serializer = new JavaScriptSerializer();
            // ViewBag.InitialData = serializer.Serialize(patientinfo); 
            if (patientinfo == null)
            {
                return HttpNotFound();
            }
            //return View(patientinfo);
            return Json(patientinfo, JsonRequestBehavior.AllowGet);
        }

        // POST: /ePatient/Edit/5
       [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(PatientInputModel patient)
        {
            if (ModelState.IsValid)
            {
                //SmartCard card = new SmartCard();
                //card.PersonID = patient.PatientID;
                //card.CSN = patient.CSN;
                //card.issueDate = patient.issueDate;
                //ManagePatient.Instance.EditSmartCard(card);

                ManagePatient.Instance.EditPatient(patient);

                //db.Entry(patient).State = EntityState.Modified;
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(patient);
        }

        // GET: /ePatient/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Patient patient = db.Patients.Find(id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            return View(patient);
        }

        // POST: /ePatient/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Patient patient = db.Patients.Find(id);
            db.Patients.Remove(patient);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Search()
        {
            return View();
        }

        #region Code for Jquery Search Grid
        //public JsonResult PatientSearch(string Fname,string Lname,string phone,string csn)
        //{
        //    //PatientSearchModel _patients = new PatientSearchModel();
        //    Fname = "Alex";
        //    double dbCsn;
        //    if (csn != "")
        //        dbCsn = Convert.ToDouble(csn);
        //    else
        //        dbCsn = Convert.ToDouble(0);

        //   List<PatientSearchList> list = ManagePatient.Instance.SearchPatient(Fname, Lname, phone, dbCsn);

        //    return Json(list, JsonRequestBehavior.AllowGet);
        //}
        #endregion

        #region Search code for KO search
        public JsonResult PatientSearch(string Fname, string Lname, string phone, string csn)
        {
            Fname = "Al";
            double dbCsn;
            if (csn != "")
                dbCsn = Convert.ToDouble(csn);
            else
                dbCsn = Convert.ToDouble(0);

            var searchedlist = ManagePatient.Instance.SearchPatient(Fname, Lname, phone, dbCsn); 

            return Json(new { Items = searchedlist });
        }
        public JsonResult PatientList()
        {
            string Fname = "";
            string Lname = "";
            string phone = "";
            string csn = "";

            Fname = "Al";
            double dbCsn;
            if (csn != "")
                dbCsn = Convert.ToDouble(csn);
            else
                dbCsn = Convert.ToDouble(0);

            var searchedlist = ManagePatient.Instance.SearchPatient(Fname, Lname, phone, dbCsn);

            return Json(new { Items = searchedlist });
        }
        #endregion


        [HttpPost]
        public JsonResult ProcessForm(PatientInputModel patient)
        {
            
            Thread.Sleep(2000);
            if(patient.PatientID==0) //Add New Patient
            {
                if (ManagePatient.Instance.CreatePatient(patient))
                    return Json(new { success = "1", context = "Add" });
                else
                    return Json(new { success = "0", context = "Add" }); 
            }
            else //Edit Patient
            {
                if (ManagePatient.Instance.EditPatient(patient))
                    return Json(new { success = "1", context = "Edit" });
                else
                    return Json(new { success = "0", context = "Edit" }); 
            }
        }

        
        //[HttpPost]
        //public string ProcessForm2(string test)
        //{
        //    return "<h2>Hi!</h2>" + test;
        //}
    }
}
