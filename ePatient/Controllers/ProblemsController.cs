﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePatient.DAL;
using ePatient.Models;

namespace ePatient.Controllers
{
    public class ProblemsController : BaseController
    {
        private ePatientDB db = new ePatientDB();

        // GET: Problems
        public async Task<ActionResult> Index()
        {
            var problems = db.Problems.Include(p => p.Patient);
            return View(await problems.ToListAsync());
        }

        // GET: Problems/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Problem problem = await db.Problems.FindAsync(id);
            if (problem == null)
            {
                return HttpNotFound();
            }
            return View(problem);
        }

        // GET: Problems/Create/patientid
        public ActionResult Create(int? id)
        {
            //ViewBag.PatientID = new SelectList(db.Patients, "PatientID", "Title");
            if (id != null)
            {
                //ViewBag.PatientID = patientid;
                Patient patient = db.Patients.Find(id);
                Problem _problem = new Problem();
                _problem.PatientID = id.Value;
                _problem.Patient = patient;

                IEnumerable<Organ> organTypes = Enum.GetValues(typeof(Organ))
                                                       .Cast<Organ>();
                _problem.OrganList = from organ in organTypes
                                    select new SelectListItem
                                    {
                                        Text = organ.ToString(),
                                        Value = ((int)organ).ToString()
                                    };

               return View(_problem);
            }
            else
            {
                return HttpNotFound();
            }
            
        }

        // POST: Problems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ProblemID,PatientID,Organs,Details,ReportedOn")] Problem problem)
        {
            if (ModelState.IsValid)
            {
                db.Problems.Add(problem);
                await db.SaveChangesAsync();
                return RedirectToAction("Edit", "ePatient", new { id = problem.PatientID });
            }
            Patient patient = db.Patients.Find(problem.PatientID);
            problem.Patient = patient;

            //ViewBag.PatientID = new SelectList(db.Patients, "PatientID", "Title", problem.PatientID);
            return View(problem);
        }

        // GET: Problems/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Problem problem = await db.Problems.FindAsync(id);
            if (problem == null)
            {
                return HttpNotFound();
            }
           // ViewBag.PatientID = new SelectList(db.Patients, "PatientID", "Title", problem.PatientID);
            IEnumerable<Organ> organTypes = Enum.GetValues(typeof(Organ))
                                                     .Cast<Organ>();
            problem.OrganList = from organ in organTypes
                                select new SelectListItem
                                {
                                    Text = organ.ToString(),
                                    Value = ((int)organ).ToString()
                                };
            

            ViewBag.PatientID = problem.PatientID;
            Patient patient = db.Patients.Find(problem.PatientID);
            problem.PatientID = problem.PatientID;
            problem.Patient = patient;

           

            return View(problem);
        }

        // POST: Problems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ProblemID,PatientID,Organs,Details,ReportedOn")] Problem problem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(problem).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Edit", "ePatient", new { id=problem.PatientID});
            }
            ViewBag.PatientID = new SelectList(db.Patients, "PatientID", "Title", problem.PatientID);

            return View(problem);
        }

        // GET: Problems/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Problem problem = await db.Problems.FindAsync(id);
            if (problem == null)
            {
                return HttpNotFound();
            }

            Patient patient = db.Patients.Find(problem.PatientID);
            problem.PatientID = problem.PatientID;
            problem.Patient = patient;

            return View(problem);
        }

        // POST: Problems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Problem problem = await db.Problems.FindAsync(id);
            db.Problems.Remove(problem);
            await db.SaveChangesAsync();
            return RedirectToAction("Edit", "ePatient", new { id = problem.PatientID });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
